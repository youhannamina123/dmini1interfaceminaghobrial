import java.awt.BorderLayout;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.util.Random;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JList;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JSlider;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Point;
import java.awt.Dimension;
import javax.swing.border.MatteBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.BevelBorder;
import java.awt.Window.Type;
import javax.swing.JProgressBar;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;

/**
 * jeu de mise permettant de miser avec de l'argent irreel
 * sur un d� (1 chance sur 6), si gagne alors le user gagner 5x ce qu'il a mis.
 * n'importe quel montant peut etre miser grace � plusieurs boutons radios. 
 * 
 * @author Mina Ghobrial
 *
 */
public class AppJeuMise extends JFrame {

	
	private int montantActuel=0;
	private int montantPrecedent=0;
	private int mise=0;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JPanel contentPane;
	private JLabel lblMontantEcriture;
	
	private JRadioButton radioAutre;
	private JSpinner spinnerPerso;
	private int deGuess=0;
	private Random generateur;
	private JTextArea txtAreaLog = new JTextArea();
	private JLabel lblArgent;
	private JProgressBar progressArgent;
	private JLabel lblTaille;
	private JScrollPane scrlLog;
	private JRadioButton radioMiseTout;
	private JRadioButton radioMiserUn;
	
	
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AppJeuMise frame = new AppJeuMise();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * creation de la fenetre 
	 */
	public AppJeuMise() {
		setBackground(Color.GRAY);
		setForeground(new Color(0, 0, 255));
		setTitle("Jeu De Mise");
		setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
		montantActuel=10;
		mise=1;
		txtAreaLog.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		txtAreaLog.append("vous avez choisi le numero 1 \n");
		txtAreaLog.append("vous misez 1$ ?\n");
		this.generateur=new Random();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(639, 100, 643, 462);
		
		JMenuBar mnuDeInterface = new JMenuBar();
		setJMenuBar(mnuDeInterface);
		
		JMenu mnuBoutton = new JMenu("Menu");
		mnuDeInterface.add(mnuBoutton);
		
		JMenuItem mnuItemAbout = new JMenuItem("A propos");
		mnuItemAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Jeu de Mise \n Version : 1.0.0 \n � Copyright � ", "Information", JOptionPane.PLAIN_MESSAGE);
			}
		});
		mnuBoutton.add(mnuItemAbout);
		
		JMenuItem mnuItemQuitter = new JMenuItem("Quitter");
		mnuItemQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mnuBoutton.add(mnuItemQuitter);
		
		JMenu mnuAffichage = new JMenu("Affichage");
		mnuDeInterface.add(mnuAffichage);
		
		JMenuItem mnuItemNuit = new JMenuItem("Nuit");
		mnuItemNuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.LIGHT_GRAY);
			}
		});
		mnuItemNuit.setSelected(true);
		mnuAffichage.add(mnuItemNuit);
		
		JMenuItem mnuItemJour = new JMenuItem("Jour");
		mnuItemJour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.white);
			}
		});
		mnuAffichage.add(mnuItemJour);
		contentPane = new JPanel();
		contentPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblMontantEcriture = new JLabel("Votre Porte Feuille = ");
		lblMontantEcriture.setFont(new Font("Nirmala UI", Font.BOLD | Font.ITALIC, 11));
		lblMontantEcriture.setBorder(new LineBorder(new Color(0, 0, 0), 6, true));
		lblMontantEcriture.setHorizontalAlignment(SwingConstants.CENTER);
		lblMontantEcriture.setBounds(29, 24, 221, 52);
		contentPane.add(lblMontantEcriture);
		
		JComboBox cboxDice = new JComboBox();
		cboxDice.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		cboxDice.setBorder(new LineBorder(new Color(0, 0, 0)));
		cboxDice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deGuess=Integer.parseInt(cboxDice.getSelectedItem().toString());
				//System.out.println(deGuess);
				txtAreaLog.append("vous avez choisi le numero "+deGuess+" de la facette du d� \n");
			}
		});
		cboxDice.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6"}));
		cboxDice.setBounds(441, 49, 153, 43);
		contentPane.add(cboxDice);
		
		spinnerPerso = new JSpinner();
		spinnerPerso.setEnabled(false);
		spinnerPerso.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		spinnerPerso.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				//System.out.println("votre mise est de "+spinnerPerso.getValue()+" ?");
				
					txtAreaLog.append("voulez-vous misez : " +spinnerPerso.getValue() + "$ ? \n");
					mise=(int) spinnerPerso.getValue();
					
					
	
			}
		});
		spinnerPerso.setModel(new SpinnerNumberModel(1, 1, 10, 1));
		spinnerPerso.setBounds(6, 221, 61, 52);
		contentPane.add(spinnerPerso);
		
		JLabel lblMiseInfo = new JLabel("Votre Mise ");
		lblMiseInfo.setBounds(6, 196, 61, 14);
		contentPane.add(lblMiseInfo);
		
		radioMiseTout = new JRadioButton("ALL-IN");
		radioMiseTout.setBorder(UIManager.getBorder("DesktopIcon.border"));
		radioMiseTout.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				spinnerPerso.setEnabled(false);
				mise=montantActuel;
				txtAreaLog.append("vous misez tout votre porte feuille : "+montantActuel+"$\n");
				
			}
		});
		buttonGroup.add(radioMiseTout);
		radioMiseTout.setBounds(6, 131, 109, 23);
		contentPane.add(radioMiseTout);
		
		radioMiserUn = new JRadioButton("1$");
		radioMiserUn.setBorder(UIManager.getBorder("DesktopIcon.border"));
		radioMiserUn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				spinnerPerso.setEnabled(false);
				
				mise=1;
				txtAreaLog.append("vous misez 1$ ? \n");
			}
		});
		radioMiserUn.setSelected(true);
		buttonGroup.add(radioMiserUn);
		radioMiserUn.setBounds(6, 94, 109, 23);
		contentPane.add(radioMiserUn);
		
		JLabel lblDeInfo = new JLabel("Le D\u00E9");
		lblDeInfo.setHorizontalAlignment(SwingConstants.CENTER);
		lblDeInfo.setBounds(451, 24, 143, 14);
		contentPane.add(lblDeInfo);
		
		radioAutre = new JRadioButton("Autre");
		radioAutre.setBorder(UIManager.getBorder("DesktopIcon.border"));
		radioAutre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				spinnerPerso.setEnabled(true);
				mise=(int)spinnerPerso.getValue();
				//System.out.println(mise);
				//txtAreaLog.append("vous misez : "+spinnerPerso.getValue()+"$ \n");
			}
		});
		buttonGroup.add(radioAutre);
		radioAutre.setBounds(6, 169, 109, 23);
		contentPane.add(radioAutre);
		
		JButton btnConfirmer = new JButton("Confirmer La Mise");
		btnConfirmer.setForeground(Color.GREEN);
		btnConfirmer.setFont(new Font("Tahoma", Font.PLAIN, 23));
		btnConfirmer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					int de=generateur.nextInt(7);
					

					if(de==deGuess) {
						//System.out.println("reussi");
						
						montantActuel=montantActuel+mise*5;
						if(montantActuel<=10) {
							progresse(montantActuel*10);
						} else {
							progresse(100);
							
						}
						
						lblArgent.setText(montantActuel+" $");
						txtAreaLog.append("vous avez gagne : "+mise*5+" $\n");
						spinnerPerso.setModel(new SpinnerNumberModel(1, 1, montantActuel, 1));
					} else {
						//System.out.println("rate");
						
						montantActuel=montantActuel-mise;
						if(montantActuel<=10) {
							progresse(montantActuel*10);
						} else {
							progressArgent.setValue(100);
						}
						lblArgent.setText(montantActuel+" $");
						txtAreaLog.append("vous avez perdu : "+mise+" $\n");
						if(montantActuel>0) {
							spinnerPerso.setModel(new SpinnerNumberModel(1, 1, montantActuel, 1));
						}
						
					}
					
					if(montantActuel<=0) {
						
						txtAreaLog.append("vous avez perdu \n Une nouvelle Partie vient de commencer ! \n 10$ ont ete rajoutes \n");
						lblArgent.setText("10");
						montantActuel=Integer.parseInt(lblArgent.getText());
						montantPrecedent=0;
						progressArgent.setValue(100);
						
					}
					
					radioMiserUn.setSelected(true);
					txtAreaLog.append("vous misez 1$ ?\n");

					//System.out.println(montantActuel);

				
		}});
		btnConfirmer.setBounds(370, 103, 224, 78);
		contentPane.add(btnConfirmer);
		
		scrlLog = new JScrollPane();
		
		
		scrlLog.setAutoscrolls(true);
		scrlLog.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrlLog.setBounds(138, 212, 456, 157);
		contentPane.add(scrlLog);
		
		
		scrlLog.setViewportView(txtAreaLog);
		
		lblArgent = new JLabel("10 $");
		lblArgent.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblArgent.setPreferredSize(new Dimension(24, 14));
		lblArgent.setHorizontalAlignment(SwingConstants.CENTER);
		lblArgent.setBounds(177, 24, 80, 48);
		
		lblTaille = new JLabel("60");
		lblTaille.setHorizontalAlignment(SwingConstants.CENTER);
		lblTaille.setFont(new Font("Tahoma", Font.PLAIN, 29));
		lblTaille.setBounds(222, 165, 69, 45);
		contentPane.add(lblTaille);
	
		
		contentPane.add(lblArgent);
		
		JSlider sliderTaille = new JSlider();
		sliderTaille.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		sliderTaille.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				int taille = sliderTaille.getValue();
				lblArgent.setFont(new Font("Tahoma", Font.PLAIN, taille/4));
				lblTaille.setText(taille+"");
				
				
			}
		});
		sliderTaille.setSnapToTicks(true);
		sliderTaille.setName("");
		sliderTaille.setPaintTicks(true);
		sliderTaille.setPaintLabels(true);
		sliderTaille.setFont(new Font("Stencil", Font.PLAIN, 11));
		sliderTaille.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		sliderTaille.setMajorTickSpacing(10);
		sliderTaille.setMinorTickSpacing(5);
		sliderTaille.setMinimum(40);
		sliderTaille.setMaximum(70);
		sliderTaille.setValue(60);
		sliderTaille.setBounds(136, 101, 224, 59);
		contentPane.add(sliderTaille);
		
		progressArgent = new JProgressBar();
		progressArgent.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				
				
				
			}
		});
		progressArgent.setStringPainted(true);
		progressArgent.setForeground(Color.BLUE);
		progressArgent.setValue(100);
		progressArgent.setBounds(6, 302, 122, 52);
		contentPane.add(progressArgent);
		
		JLabel lblTailleInfo = new JLabel("Taille : ");
		lblTailleInfo.setForeground(Color.BLUE);
		lblTailleInfo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblTailleInfo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTailleInfo.setBounds(134, 169, 116, 31);
		contentPane.add(lblTailleInfo);
		
		
		
	}
	/**
	 * fonction qui permet de faire progresser la barre de progression 
	 * en indiquant un parametre
	 * @param pourcentage de la progression (argent profit)
	 */
	public void progresse(int pourcentage) {
		progressArgent.setValue(pourcentage);
	}
	/**
	 * permet de retourner le montant actuel
	 * @return le montant du portefeuille
	 */

	private int getMontantActuel() {
		return montantActuel;
	}
	/**
	 * permet de modifier le montant actuel
	 * @param montantActuel montant du portefeuille a modifier
	 */
	private void setMontantActuel(int montantActuel) {
		this.montantActuel = montantActuel;
	}
}
