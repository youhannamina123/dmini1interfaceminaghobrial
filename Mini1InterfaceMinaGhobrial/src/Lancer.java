import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * classe permet de lancer AppJeuMise
 * 
 * @author Mina Ghobrial
 *
 */
public class Lancer extends JFrame {

	private JPanel contentPane;
	private AppJeuMise AppJeuMise=new AppJeuMise();
	private static Lancer frame = new Lancer();

	/**
	 * lancement de la page de lancement de AppJeuMise
	 * @param args s
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//Lancer frame = new Lancer();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * cr�ation de la frame de la page de lancement 
	 */
	public Lancer() {
		setTitle("Lancement");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(735, 390, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnLancer = new JButton("Lancer La Partie ");
		btnLancer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				AppJeuMise.setVisible(true);
				
				
				
			}
		});
		btnLancer.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnLancer.setBounds(44, 53, 329, 114);
		contentPane.add(btnLancer);
	}
}
